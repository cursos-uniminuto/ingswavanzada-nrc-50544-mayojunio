
# inportar flask
from flask import Flask, render_template, url_for, redirect

# creamos la aplicación teniendo en cuenta una variable
app = Flask(__name__)

# establecer las rutas para la aplicación
@app.route('/') # / es la ruta inicial del proyecto
# asigno la ruta a una función
def index():
    # return "<br><br><br><h1>Aprendiendo Web con Flask y Python...</h1>"
    return render_template('index.html')

@app.route('/estudiantes') # / es la ruta inicial del proyecto
# asigno la ruta a una función
def estudiantes():
    # return "<br><br><br><h1>Web Page Estudiantes...</h1>"
    return render_template('estudiantes.html')

@app.route('/uniminuto') # / es la ruta inicial del proyecto
# asigno la ruta a una función
def uniminuto():
    # return "<br><br><br><h1>Web Page Uniminuto...</h1>"
    return render_template('uniminuto.html')

# identificar el fichero principal
if __name__ == '__main__':
    app.run(debug = True)   # reiniciar el servidor automáticamente

