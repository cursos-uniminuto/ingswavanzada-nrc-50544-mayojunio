
<h1>Command line instructions</h1>

You can also upload existing files from your computer using the instructions below. <br>

<h2>Git global setup

git config --global user.name "Jahir Saavedra" <br>
git config --global user.email "jahir.saavedra@uniminuto.edu" <br>

<h2>Create a new repository</h2>

git clone https://gitlab.com/cursos-uniminuto/ingswavanzada-nrc-50544-mayojunio.git <br>
cd ingswavanzada-nrc-50544-mayojunio <br>
git switch --create main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push --set-upstream origin main <br>

<h2>Push an existing folder</h2>

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/cursos-uniminuto/ingswavanzada-nrc-50544-mayojunio.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push --set-upstream origin main <br>

<h2>Push an existing Git repository</h2>

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/cursos-uniminuto/ingswavanzada-nrc-50544-mayojunio.git <br>
git push --set-upstream origin --all <br>
git push --set-upstream origin --tags <br>